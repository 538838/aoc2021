package d17

import (
	"strings"
	"testing"
)

var example = `target area: x=20..30, y=-10..-5`

// TestExample tests the solution against the example
func TestExample(t *testing.T) {
	input := strings.NewReader(example)
	sola, solb := Run(input)
	// Verify Solution A
	if a, ok := sola.(int); ok {
		if a != 45 {
			t.Errorf("Expected %d, got %d", 45, a)
		}
	} else {
		t.Errorf("Expected int, got %T", sola)
	}
	// Verify Solution B
	if b, ok := solb.(int); ok {
		if b != 112 {
			t.Errorf("Expected %d, got %d", 112, b)
		}
	} else {
		t.Errorf("Expected int, got %T", solb)
	}
}
