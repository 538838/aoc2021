package d17

import (
	"aoc2021/d1"
	"aoc2021/d6"
	"aoc2021/d7"
	"io"
	"strings"
)

// Run runs the program and returns the results
func Run(input io.Reader) (resA, resB interface{}) {
	// Parse input to target area limits
	line := d6.MustReadAll(input)[len("target area: "):]
	lims := strings.Split(line, ",")
	_, xMin, xMas := ParseRange(lims[0])
	_, yMin, yMax := ParseRange(lims[1])

	// Calculate limits to velocity limits.
	vxMin := 0
	for d7.SumToN(vxMin) < xMin {
		vxMin++
	}
	vxMax := xMas
	vyMin := yMin
	vyMax := -yMin - 1 // Y trajectory up is symmetrical.

	// Find y velocities that hits the target
	maxSteps := 0
	ySteps := make(map[int][]int)
	for vy0 := vyMin; vy0 <= vyMax; vy0++ {
		y := 0
		steps := 0
		for vy := vy0; y >= yMin; vy-- {
			steps++
			y += vy
			if y >= yMin && y <= yMax {
				ySteps[steps] = append(ySteps[steps], vy0)
			}
		}
		if steps >= maxSteps {
			maxSteps = steps
		}
	}

	// Find x velocities that hits the target.
	hitsTarget := make(map[[2]int]struct{})
	for vx0 := vxMin; vx0 <= vxMax; vx0++ {
		x := 0
		steps := 0
		for vx := vx0; steps <= maxSteps; vx-- {
			steps++
			if vx > 0 {
				x += vx
			}
			if x >= xMin && x <= xMas {
				// Hits target. Find all y velocities that also hits the target for this step.
				for _, vy0 := range ySteps[steps] {
					hitsTarget[[2]int{vx0, vy0}] = struct{}{}
				}
			}
		}
	}

	resA = d7.SumToN(vyMax)
	resB = len(hitsTarget)

	return
}

// ParseRange parses string like x=-13..34 into variable name and low and high number.
func ParseRange(s string) (string, int, int) {
	varVals := strings.Split(s, "=")
	if len(varVals) != 2 {
		panic("Expects a string like 'x=-13..34'")
	}
	vals := d1.ParseInts(strings.Split(varVals[1], ".."), 10)
	if len(vals) != 2 {
		panic("Expects a string like 'x=-13..34'")
	}
	return varVals[0], vals[0], vals[1]
}
