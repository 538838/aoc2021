package d16

import (
	"strings"
	"testing"
)

var exampleA = `A0016C880162017C3686B18A3D4780`
var exampleB = `9C0141080250320F1802104A08`

// TestExample tests the solution against the example
func TestExample(t *testing.T) {
	inputA := strings.NewReader(exampleA)
	inputB := strings.NewReader(exampleB)
	sola, _ := Run(inputA)
	_, solb := Run(inputB)
	// Verify Solution A
	if a, ok := sola.(int); ok {
		if a != 31 {
			t.Errorf("Expected %d, got %d", 31, a)
		}
	} else {
		t.Errorf("Expected int, got %T", sola)
	}
	// Verify Solution B
	if b, ok := solb.(int); ok {
		if b != 1 {
			t.Errorf("Expected %d, got %d", 1, b)
		}
	} else {
		t.Errorf("Expected int, got %T", solb)
	}
}
