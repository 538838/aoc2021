package d16

import (
	"aoc2021/d14"
	"aoc2021/d6"
	"fmt"
	"io"
	"strconv"
	"strings"
)

// Run runs the program and returns the results
func Run(input io.Reader) (resA, resB interface{}) {
	d14.AssertInt64()
	// Parse input to strings of '0' and '1'.
	// A more proper type would probably be a byte/int array with bit manipulation,
	// but this is good enough (and faster to solve) for this problem.
	line := d6.MustReadAll(input)
	var sb strings.Builder
	for i := 0; i < len(line); i++ {
		i := mustAtoi64(line[i:i+1], 16)
		sb.WriteString(fmt.Sprintf("%04b", i))
	}

	// Parse packet
	pack := parsePacket(sb.String())
	resA = pack.Version
	resB = pack.Value

	return
}

// packet is a parsed packet
type packet struct {
	Version int
	Length  int
	Value   int
}

// parsePacket parses a packet from a string of '0' or '1'.
func parsePacket(bits string) packet {
	if len(bits) < 8 {
		return packet{-1, len(bits), 0}
	}
	// Parse header
	version := mustAtoi64(bits[0:3], 2)
	packType := mustAtoi64(bits[3:6], 2)
	i := 6

	// Literal value
	if packType == 4 {
		str := ""
		str += bits[i+1 : i+5]
		for i += 5; bits[i-5] == '1'; i += 5 {
			str += bits[i+1 : i+5]
		}
		return packet{version, i, mustAtoi64(str, 2)}
	}

	// Operator packets
	operators := map[int]func(ii []int) int{
		0: sum, 1: product, 2: minimum, 3: Maximum,
		5: greaterThan, 6: lessThan, 7: equalTo,
	}
	// Parse sub packets
	var subPackets []packet
	if bits[i] == '0' {
		l := mustAtoi64(bits[i+1:i+16], 2)
		i += 16
		subPackets = parsePackets(bits[i:i+l], 0)
	} else {
		c := mustAtoi64(bits[i+1:i+12], 2)
		i += 12
		subPackets = parsePackets(bits[i:], c)
	}
	// Get data from sub packets and return the value from the correct operator
	var values []int
	for _, p := range subPackets {
		i += p.Length
		if p.Version == -1 {
			continue
		}
		version += p.Version
		values = append(values, p.Value)
	}
	return packet{version, i, operators[packType](values)}
}

// parsePacket parses all packets from a string of '0' or '1', with an optional
// max number of packets.
func parsePackets(bits string, maxPackets int) (packets []packet) {
	for i, cnt := 0, 0; i < len(bits) && (maxPackets == 0 || cnt < maxPackets); cnt++ {
		p := parsePacket(bits[i:])
		packets = append(packets, p)
		i += p.Length
	}
	return packets
}

// IntMax is the largest value of signed integer type
const IntMax = int(^uint(0) >> 1)

// IntMin is the smalest value of signed integer type
const IntMin = -IntMax - 1

// sum returns the sum of all input numbers.
func sum(ii []int) int {
	res := 0
	for _, v := range ii {
		res += v
	}
	return res
}

// product returns the product of all input numbers.
func product(ii []int) int {
	res := 1
	for _, v := range ii {
		res *= v
	}
	return res
}

// minimum returns the minimum of all input numbers.
func minimum(ii []int) int {
	res := IntMax
	for _, v := range ii {
		if v < res {
			res = v
		}
	}
	return res
}

// Maximum returns the maximum of all input numbers.
func Maximum(ii []int) int {
	res := IntMin
	for _, v := range ii {
		if v > res {
			res = v
		}
	}
	return res
}

// greaterThan returns 1 if first input is greater than second input, otherwise 0.
func greaterThan(ii []int) int {
	if len(ii) != 2 {
		panic("Input must have length 2")
	}
	if ii[0] > ii[1] {
		return 1
	}
	return 0
}

// lessThan returns 1 if first input is less than second input, otherwise 0.
func lessThan(ii []int) int {
	if len(ii) != 2 {
		panic("Input must have length 2")
	}
	if ii[0] < ii[1] {
		return 1
	}
	return 0
}

// equalTo returns 1 if first input is equal to second input, otherwise 0.
func equalTo(ii []int) int {
	if len(ii) != 2 {
		panic("Input must have length 2")
	}
	if ii[0] == ii[1] {
		return 1
	}
	return 0
}

// mustAtoi64 converts string to int, or panics.
// Assumes int = int64, but this must be verified by the caller.
func mustAtoi64(s string, base int) int {
	i, err := strconv.ParseInt(s, base, 64)
	if err != nil {
		panic(err)
	}
	return int(i)
}
