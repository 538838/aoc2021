package d7

import (
	"aoc2021/d1"
	"aoc2021/d6"
	"fmt"
	"io"
	"sort"
	"strings"
)

// Run runs the program and returns the results
func Run(input io.Reader) (resA, resB interface{}) {
	s := strings.Split(d6.MustReadAll(input), ",")
	dist := d1.ParseInts(s, 10)
	sort.Ints(dist)

	// A: Min of sum of absolut value occurs at the median
	var midA int = (len(dist) - 1) / 2
	median := dist[midA]
	h1 := dist[:midA]
	h2 := dist[midA:]
	resA = (len(h1)*median - d6.IntSum(h1)) + (d6.IntSum(h2) - len(h2)*median)

	// B: Assuming min for sum of squares, which occurs at the mean, and iterate
	midB := (d6.IntSum(dist) + len(dist)/2) / len(dist)
	dir := 1
	curr := 0
	for dir >= -1 {
		curr = costB(dist, midB)
		if costB(dist, midB+dir) < curr {
			midB += dir
		} else {
			dir -= 2
		}
	}
	resB = curr

	return
}

// Calculate total cost for part b
func costB(dist []int, to int) int {
	cost := 0
	for _, d := range dist {
		diff := d - to
		if diff < 0 {
			diff = -diff
		}
		cost += SumToN(diff)
	}
	return cost
}

// SumToN returns the sum from 0 to n.
func SumToN(n int) int {
	if n < 0 {
		panic(fmt.Sprintf("Can not calculate sum from 0 to %d", n))
	}
	if n%2 == 0 {
		return (1 + n) * (n / 2)
	}
	m := n - 1
	return (1+m)*(m/2) + n
}
