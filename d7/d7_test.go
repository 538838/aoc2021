package d7

import (
	"strings"
	"testing"
)

var example = `16,1,2,0,4,2,7,1,2,14`

// TestExample tests the solution against the example
func TestExample(t *testing.T) {
	input := strings.NewReader(example)
	sola, solb := Run(input)
	// Verify Solution A
	if a, ok := sola.(int); ok {
		if a != 37 {
			t.Errorf("Expected %d, got %d", 37, a)
		}
	} else {
		t.Errorf("Expected int, got %T", sola)
	}
	// Verify Solution B
	if b, ok := solb.(int); ok {
		if b != 168 {
			t.Errorf("Expected %d, got %d", 168, b)
		}
	} else {
		t.Errorf("Expected int, got %T", solb)
	}
}
