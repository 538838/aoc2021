package d22

import (
	"aoc2021/d1"
	"aoc2021/d14"
	"aoc2021/d17"
	"aoc2021/d19"
	"fmt"
	"io"
	"strings"
)

// Run runs the program and returns the results
func Run(input io.Reader) (resA, resB interface{}) {
	// Assume int64, otherwise the result will overflow
	d14.AssertInt64()

	// Parse lines to list of cuboids in init region and list of cuboids in total
	lines := d1.ReadLines(input)
	var init []cuboid
	var reboot []cuboid
	initRegion := cuboid{0, d19.Point{X: -50, Y: -50, Z: -50}, d19.Point{X: 51, Y: 51, Z: 51}}
	for i := len(lines) - 1; i >= 0; i-- {
		l := lines[i]
		var val int
		if strings.HasPrefix(l, "on ") {
			l = l[len("on "):]
			val = 1
		} else if strings.HasPrefix(l, "off ") {
			l = l[len("off "):]
			val = 0
		} else {
			panic(fmt.Sprintf("Line could not be parsed: %s", l))
		}
		coor := strings.Split(l, ",")
		_, x1, x2 := d17.ParseRange(coor[0])
		_, y1, y2 := d17.ParseRange(coor[1])
		_, z1, z2 := d17.ParseRange(coor[2])
		c := cuboid{
			Val: val,
			P1:  d19.Point{X: x1, Y: y1, Z: z1},
			P2:  d19.Point{X: x2 + 1, Y: y2 + 1, Z: z2 + 1},
		}
		if c.overlaps(initRegion) {
			init = append(init, c)
		}
		reboot = append(reboot, c)
	}

	// Apply the cuboid lists and count the total volume
	v := 0
	for _, c := range applyCubeoidList(init) {
		v += c.volume()
	}
	resA = v
	v = 0
	for _, c := range applyCubeoidList(reboot) {
		v += c.volume()
	}
	resB = v
	return
}

// applyCubeoidList applies a list of cuboids one by one, in reverse order,
// and returns a list of non-overlapping cuboids with value not equal to 0.
func applyCubeoidList(input []cuboid) (result []cuboid) {
outer:
	for len(input) != 0 {
		curr := input[len(input)-1]
		input = input[:len(input)-1]
		if curr.Val != 0 {
			// If overlap, split the current cuboid and retry
			for _, r := range result {
				if curr.overlaps(r) {
					input = append(input, curr.subtract(r)...)
					continue outer
				}
			}
			result = append(result, curr)
		} else {
			// Find overlap which should be set to 0
			for i := len(result) - 1; i >= 0; i-- {
				if curr.overlaps(result[i]) {
					r := result[i]
					result[i] = result[len(result)-1]
					result = result[:len(result)-1]
					result = append(result, r.subtract(curr)...)
				}
			}
		}
	}
	return
}

// cuboid is a 3D structure, with a value, between P1 (inclusive) and P2 (exclusive).
type cuboid struct {
	Val int
	P1  d19.Point
	P2  d19.Point
}

// isValid returns true if this cuboid is valid
func (c cuboid) isValid() bool {
	return c.P1.X < c.P2.X &&
		c.P1.Y < c.P2.Y &&
		c.P1.Z < c.P2.Z
}

// overlaps returns true if the cuboids overlap each other
func (c cuboid) overlaps(c2 cuboid) bool {
	return !(c.P2.X <= c2.P1.X || c.P1.X >= c2.P2.X ||
		c.P2.Y <= c2.P1.Y || c.P1.Y >= c2.P2.Y ||
		c.P2.Z <= c2.P1.Z || c.P1.Z >= c2.P2.Z)
}

// size returns the volume of the cuboid
func (c cuboid) volume() int {
	return (c.P2.X - c.P1.X) * (c.P2.Y - c.P1.Y) * (c.P2.Z - c.P1.Z)
}

// overlap returns the cuboid that is the overlap between the cuboids.
func (c cuboid) overlap(c2 cuboid) cuboid {
	p1 := d19.Point{
		X: max(c.P1.X, c2.P1.X),
		Y: max(c.P1.Y, c2.P1.Y),
		Z: max(c.P1.Z, c2.P1.Z)}
	p2 := d19.Point{
		X: min(c.P2.X, c2.P2.X),
		Y: min(c.P2.Y, c2.P2.Y),
		Z: min(c.P2.Z, c2.P2.Z)}
	return cuboid{c.Val, p1, p2}
}

// subtract subtracts the input cube from this cube, by splitting it, and
// returns the result as a list of cuboids.
func (c cuboid) subtract(c2 cuboid) (res []cuboid) {
	ol := c.overlap(c2)
	p1 := map[int]d19.Point{-1: c.P1, 0: ol.P1, 1: ol.P2}
	p2 := map[int]d19.Point{-1: ol.P1, 0: ol.P2, 1: c.P2}
	for _, px := range []int{-1, 0, 1} {
		for _, py := range []int{-1, 0, 1} {
			for _, pz := range []int{-1, 0, 1} {
				if px == 0 && py == 0 && pz == 0 {
					continue
				}
				nc := cuboid{
					Val: c.Val,
					P1:  d19.Point{X: p1[px].X, Y: p1[py].Y, Z: p1[pz].Z},
					P2:  d19.Point{X: p2[px].X, Y: p2[py].Y, Z: p2[pz].Z},
				}
				if nc.isValid() {
					res = append(res, nc)
				}
			}
		}
	}
	return
}

// max returns the largest of the input numbers
func max(a int, b int) int {
	if a > b {
		return a
	}
	return b
}

// min returns the smallest of the input numbers
func min(a int, b int) int {
	if a < b {
		return a
	}
	return b
}
