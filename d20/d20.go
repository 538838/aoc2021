package d20

import (
	"aoc2021/d1"
	"aoc2021/d13"
	"aoc2021/d5"
	"io"
)

// Run runs the program and returns the results
func Run(input io.Reader) (resA, resB interface{}) {
	lines := d1.ReadLines(input)
	alg := []rune(lines[0])
	img := d13.MakeSparseMap()
	img.DimY[1] = len(lines[2:]) - 1
	img.DimX[1] = len(lines[2]) - 1
	for y, l := range lines[2:] {
		for x, r := range l {
			if r == '#' {
				img.Set(d5.Point{X: x, Y: y}, 1)
			}
		}
	}

	// Run algorithm 2/50 times
	pad := 0
	for i := 1; i <= 50; i++ {
		pad = runAlg(img, alg, pad)
		if i == 2 {
			resA = len(img.Val)
		}
	}
	resB = len(img.Val)

	return
}

// runAlg runs the algorithm on the input image. Positions ouside the image have pad value
func runAlg(img *d13.SparseMap, alg []rune, pad int) int {
	upd := make(map[d5.Point]int)
	for y := img.DimY[0] - 1; y <= img.DimY[1]+1; y++ {
		for x := img.DimX[0] - 1; x <= img.DimX[1]+1; x++ {
			v := 0
			for i := 0; i < 9; i++ {
				p := d5.Point{X: x - 1 + (i % 3), Y: y - 1 + (i / 3)}
				if p.X < img.DimX[0] || p.X > img.DimX[1] || p.Y < img.DimY[0] || p.Y > img.DimY[1] {
					v = (v << 1) + pad
				} else {
					v = (v << 1) + img.Val[p]
				}
			}
			if alg[v] == '#' {
				upd[d5.Point{X: x, Y: y}] = 1
			}
		}
	}
	img.DimX = [2]int{img.DimX[0] - 1, img.DimX[1] + 1}
	img.DimY = [2]int{img.DimY[0] - 1, img.DimY[1] + 1}
	img.Val = upd
	if pad == 0 && alg[0] == '#' {
		return 1
	} else if pad == 1 && alg[511] == '.' {
		return 0
	}
	return pad
}
