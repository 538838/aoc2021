package d14

import (
	"strings"
	"testing"
)

var example = `NNCB

CH -> B
HH -> N
CB -> H
NH -> C
HB -> C
HC -> B
HN -> C
NN -> C
BH -> H
NC -> B
NB -> B
BN -> B
BB -> N
BC -> B
CC -> N
CN -> C`

// TestExample tests the solution against the example
func TestExample(t *testing.T) {
	input := strings.NewReader(example)
	sola, solb := Run(input)
	// Verify Solution A
	if a, ok := sola.(int); ok {
		if a != 1588 {
			t.Errorf("Expected %d, got %d", 1588, a)
		}
	} else {
		t.Errorf("Expected int, got %T", sola)
	}
	// Verify Solution B
	if b, ok := solb.(int); ok {
		if b != 2188189693529 {
			t.Errorf("Expected %d, got %d", 2188189693529, b)
		}
	} else {
		t.Errorf("Expected int, got %T", solb)
	}
}
