package d14

import (
	"aoc2021/d1"
	"io"
	"reflect"
	"strings"
)

// Run runs the program and returns the results
func Run(input io.Reader) (resA, resB interface{}) {
	AssertInt64()

	// Parse input pairs, adding a fake pair last to count last rune correctly
	lines := d1.ReadLines(input)
	template := lines[0] + "#"
	pairs := make(map[runePair]int)
	for i := 0; i < len(template)-1; i++ {
		pairs[stringToRunePair(template[i:i+2])]++
	}
	// Parse input rules
	rules := make(map[runePair][2]runePair)
	for _, l := range lines[2:] {
		s := strings.Split(l, " -> ")
		r := stringToRunePair(s[0])
		r1 := runePair{r.R1, rune(s[1][0])}
		r2 := runePair{rune(s[1][0]), r.R2}
		rules[r] = [2]runePair{r1, r2}
	}

	// Run 10 and 40 steps for part A and B
	for s := 0; s < 40; s++ {
		// For each pair matching a rule, apply the rule
		newPairs := make(map[runePair]int, len(pairs))
		for p, c := range pairs {
			if r, ok := rules[p]; ok {
				newPairs[r[0]] += c
				newPairs[r[1]] += c
			} else {
				newPairs[p] = c
			}
		}
		pairs = newPairs
		if s == 9 {
			resA = quantifyRunePairs(pairs)
		}
	}
	resB = quantifyRunePairs(pairs)

	return
}

// AssertInt64 verifies that int is 64 bits or larger, or panics.
// In most cases not a good solution, but it simplifies theese solutions a bit.
func AssertInt64() {
	var i int
	if reflect.TypeOf(i).Size() < 8 {
		panic("Using int and assuming 64 bits")
	}
}

// runePair is a struct for two runes, R1 and R2
type runePair struct {
	R1 rune
	R2 rune
}

// stringToRunePair creates a runePair from a string.
// The string must have length 2, otherwise this function panics.
func stringToRunePair(s string) runePair {
	if len(s) != 2 {
		panic("Input string must be 2 runes long")
	}
	return runePair{rune(s[0]), rune(s[1])}
}

// quantifyRunePairs finds the most and least common rune in the pairs and
// return the difference.
func quantifyRunePairs(pairs map[runePair]int) int {
	count := make(map[rune]int)
	for p, c := range pairs {
		count[p.R1] += c
	}
	max := 0
	min := int(^uint(0) >> 1)
	for _, c := range count {
		if c > max {
			max = c
		}
		if c < min {
			min = c
		}
	}
	return max - min
}
