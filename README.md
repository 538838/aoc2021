# Advent Of Code 2021 #
This repo contains solutions for [Advent of code 2021](https://adventofcode.com/2021), written in golang.

The repo will also contain my inputs and my results.
Not sure if it's a good idea, but it makes it simpler for me :)

The whole repo is a go module to be able to in a simple way reuse code from previous days, I hope.
I'm guessing that might be an attempt to be too smart, resulting in longer time spent with each solution...
But it will be fun along the way.

If this was a real project I would move many common used functions and types to their own package
instead of letting each solution contain code used by other solutions. And in that case I would
let the private types in each solution have private members (which would make a bit more sense),
but I currently try to make it require as few changes as possible to use reuse the code in future
solutions.

The code that is used for generating the answers to each puzzle is first cleaned up a bit before
it's commited. Since I sometimes have solved the puzzle long before (days before) taking the time
to clean up the code the commit dates will not match well with when the puzzle was solved.
