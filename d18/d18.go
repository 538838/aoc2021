package d18

import (
	"aoc2021/d1"
	"aoc2021/d2"
	"io"
)

// Probably not the slimmest solution, but is was pretty fun

// Run runs the program and returns the results
func Run(input io.Reader) (resA, resB interface{}) {
	lines := d1.ReadLines(input)
	// Add all pairs
	p := parsePair(lines[0])
	for _, l := range lines[1:] {
		p = add(p, parsePair(l))
	}
	resA = p.magnitude()

	// Find max sum
	max := 0
	for i, l1 := range lines[:len(lines)-1] {
		for _, l2 := range lines[i+1:] {
			// Instead of parsing it several times there should probably have been a deep copy.
			p11 := parsePair(l1)
			p12 := parsePair(l2)
			p21 := parsePair(l1)
			p22 := parsePair(l2)
			if m := add(p11, p12).magnitude(); m > max {
				max = m
			}
			if m := add(p22, p21).magnitude(); m > max {
				max = m
			}
		}
	}
	resB = max

	return
}

// A pair is either a value or a linked pair of pairs.
type pair struct {
	Value  int
	Left   *pair
	Right  *pair
	Parent *pair
}

// parsePair creates a pair from a string representation
func parsePair(s string) *pair {
	curr := &pair{Value: 0}
	for _, r := range s {
		if r == '[' {
			curr.Left = &pair{Parent: curr, Value: 0}
			curr.Right = &pair{Parent: curr, Value: 0}
			curr = curr.Left
		} else if r == ',' {
			curr = curr.Parent.Right
		} else if r == ']' {
			curr = curr.Parent
		} else {
			curr.Value = d2.MustAtoi(string(r))
		}
	}
	return curr
}

// add two pairs together and return the reduced result
func add(p1 *pair, p2 *pair) *pair {
	p := &pair{Left: p1, Right: p2}
	p.Left.Parent = p
	p.Right.Parent = p
	p.reduce()
	return p
}

// isValue returns true if the pair is a value, and false if it's a pair of pairs.
func (p *pair) isValue() bool {
	if p.Left == nil && p.Right != nil || p.Left != nil && p.Right == nil {
		panic("Either none or both left and right must be nil")
	}
	return p.Left == nil
}

// magnitude returns the snailfish magnitude of the number
func (p *pair) magnitude() int {
	if p.isValue() {
		return p.Value
	}
	return p.Left.magnitude()*3 + p.Right.magnitude()*2
}

// findNextValue returns the closest pair to the right that is a value
func (p *pair) findRightValue() *pair {
	for p.Parent != nil && p.Parent.Right == p {
		p = p.Parent
	}
	if p.Parent == nil {
		return nil
	}
	for p = p.Parent.Right; !p.isValue(); {
		p = p.Left
	}
	return p
}

// findNextValue returns the closest pair to the left that is a value
func (p *pair) findLeftValue() *pair {
	for p.Parent != nil && p.Parent.Left == p {
		p = p.Parent
	}
	if p.Parent == nil {
		return nil
	}
	for p = p.Parent.Left; !p.isValue(); {
		p = p.Right
	}
	return p
}

// reduce reduces the pair by exploding and splitting where neccesary.
func (p *pair) reduce() {
	// This could probably have been done more effectively by having explode and split
	// return the pairs midified by them.
	for r := true; r; {
		explode(p, 0)
		r = splitLeft(p)
	}
}

// explode explodes all pairs nested inside four pairs, starting from the left most pair.
func explode(p *pair, depth int) {
	if p.isValue() {
		return
	}
	if depth == 4 {
		vl := p.Left.Value
		vr := p.Right.Value
		if pp := p.findLeftValue(); pp != nil {
			pp.Value += vl
		}
		if pn := p.findRightValue(); pn != nil {
			pn.Value += vr
		}
		p.Left = nil
		p.Right = nil
		return
	}
	explode(p.Left, depth+1)
	explode(p.Right, depth+1)
}

// splitLeft splits the left most value larger than 9.
// returns true if a value was split.
func splitLeft(p *pair) bool {
	if !p.isValue() {
		return splitLeft(p.Left) || splitLeft(p.Right)
	}
	if p.Value > 9 {
		p.Left = &pair{Parent: p, Value: p.Value / 2}
		p.Right = &pair{Parent: p, Value: (p.Value + 1) / 2}
		p.Value = 0
		return true
	}
	return false
}
