package d11

import (
	"strings"
	"testing"
)

var example = `5483143223
2745854711
5264556173
6141336146
6357385478
4167524645
2176841721
6882881134
4846848554
5283751526`

// TestExample tests the solution against the example
func TestExample(t *testing.T) {
	input := strings.NewReader(example)
	sola, solb := Run(input)
	// Verify Solution A
	if a, ok := sola.(int); ok {
		if a != 1656 {
			t.Errorf("Expected %d, got %d", 1656, a)
		}
	} else {
		t.Errorf("Expected int, got %T", sola)
	}
	// Verify Solution B
	if b, ok := solb.(int); ok {
		if b != 195 {
			t.Errorf("Expected %d, got %d", 195, b)
		}
	} else {
		t.Errorf("Expected int, got %T", solb)
	}
}
