package d11

import (
	"aoc2021/d1"
	"aoc2021/d5"
	"aoc2021/d9"
	"io"
)

// Run runs the program and returns the results
func Run(input io.Reader) (resA, resB interface{}) {
	// Parse input to map
	var vals [][]int
	for _, l := range d1.ReadLines(input) {
		vals = append(vals, d9.ParseDigits(l))
	}
	vm := d9.MakeValueMap(vals)

	flashes := 0
	for step := 1; step <= 100 || resB == nil; step++ {
		// Increase all points, recursively
		for y := 0; y < vm.DimY; y++ {
			for x := 0; x < vm.DimX; x++ {
				increasePoint(vm, x, y)
			}
		}
		// Calculate flashes
		f := 0
		for y := 0; y < vm.DimY; y++ {
			for x := 0; x < vm.DimX; x++ {
				if vm.Val[y][x] > 9 {
					vm.Val[y][x] = 0
					f++
				}
			}
		}
		flashes += f
		// Set results
		if step == 100 {
			resA = flashes
		}
		if f == vm.DimX*vm.DimY && resB == nil {
			resB = step
		}
	}

	return
}

// increasePoint increases the current point, and if the current point reaches
// 10 calls this function for each surrounding point
func increasePoint(vm *d9.ValueMap, x int, y int) {
	if val, ok := vm.Get(d5.Point{X: x, Y: y}); ok && val == 9 {
		vm.Val[y][x]++
		for dy := -1; dy <= 1; dy++ {
			for dx := -1; dx <= 1; dx++ {
				increasePoint(vm, x+dx, y+dy)
			}
		}
	} else if ok {
		vm.Val[y][x]++
	}
}
