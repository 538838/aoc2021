// 2>/dev/null || /usr/bin/env go run $0 $@; exit $?
package main

import (
	"aoc2021/d1"
	"aoc2021/d10"
	"aoc2021/d11"
	"aoc2021/d12"
	"aoc2021/d13"
	"aoc2021/d14"
	"aoc2021/d15"
	"aoc2021/d16"
	"aoc2021/d17"
	"aoc2021/d18"
	"aoc2021/d19"
	"aoc2021/d2"
	"aoc2021/d20"
	"aoc2021/d21"
	"aoc2021/d22"
	"aoc2021/d23"
	"aoc2021/d24"
	"aoc2021/d25"
	"aoc2021/d3"
	"aoc2021/d4"
	"aoc2021/d5"
	"aoc2021/d6"
	"aoc2021/d7"
	"aoc2021/d8"
	"aoc2021/d9"
	"flag"
	"fmt"
	"io"
	"os"
	"time"
)

var solutions = []func(input io.Reader) (interface{}, interface{}){
	d1.Run, d2.Run, d3.Run, d4.Run, d5.Run,
	d6.Run, d7.Run, d8.Run, d9.Run, d10.Run,
	d11.Run, d12.Run, d13.Run, d14.Run, d15.Run,
	d16.Run, d17.Run, d18.Run, d19.Run, d20.Run,
	d21.Run, d22.Run, d23.Run, d24.Run, d25.Run,
}

func main() {
	// Take argument for solution number
	sol := flag.Int("s", 0, "solution to run, or 0 for all")
	flag.Parse()
	s := *sol

	// Run specific solution or all
	if s == 0 {
		// Run all
		for i := 1; i <= len(solutions); i++ {
			run(i)
		}
	} else if s > 0 && s <= len(solutions) {
		// Run single
		run(s)
	} else {
		fmt.Fprintf(os.Stderr, "Invalid solution number %d. Valid solutions are [%d,%d].\n", s, 1, len(solutions))
		os.Exit(1)
	}
}

func run(day int) {
	input, err := os.Open(fmt.Sprintf("inputs/%d", day))
	if err != nil {
		panic(err)
	}
	t0 := time.Now()
	a, b := solutions[day-1](input)
	dt := time.Since(t0)
	fmt.Printf("[Day %d]\nA=%v\nB=%v\n", day, a, b)
	fmt.Fprintf(os.Stderr, "(Time: %v)\n", dt)
}
