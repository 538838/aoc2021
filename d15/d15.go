package d15

import (
	"aoc2021/d1"
	"aoc2021/d5"
	"aoc2021/d9"
	"io"
)

// Run runs the program and returns the results
func Run(input io.Reader) (resA, resB interface{}) {
	// Parse input to map
	var vals [][]int
	for _, l := range d1.ReadLines(input) {
		vals = append(vals, d9.ParseDigits(l))
	}
	vm := d9.MakeValueMap(vals)

	// Create a map with 5 times x-size and y-size
	vals5 := make([][]int, vm.DimY*5)
	for y := 0; y < vm.DimY*5; y++ {
		l := make([]int, vm.DimX*5)
		for x := 0; x < vm.DimX*5; x++ {
			v := vals[y%vm.DimY][x%vm.DimX] + (y / vm.DimY) + (x / vm.DimX)
			v = ((v - 1) % 9) + 1
			l[x] = v
		}
		vals5[y] = l
	}
	vm5 := d9.MakeValueMap(vals5)

	// Find lowest path cost from top left to bottom right
	start := d5.Point{X: 0, Y: 0}
	resA = findPathCost(vm, start, d5.Point{X: vm.DimX - 1, Y: vm.DimY - 1})
	resB = findPathCost(vm5, start, d5.Point{X: vm5.DimX - 1, Y: vm5.DimY - 1})

	return
}

// findPathCost finds the optimal path between start and end, using A* algorithm,
// and returns the cost. Costs is a map of costs for visiting each node.
func findPathCost(costs *d9.ValueMap, start d5.Point, end d5.Point) int {
	// A tree map ordered by estimated cost would have been a good idea for the open map,
	// but there is no build in type for it, and the current solutions is fast enough.
	open := make(map[d5.Point]node)
	closed := make(map[d5.Point]node)
	n0 := makeNode(start, 0, manhattanDistance(start, end))
	open[n0.Point] = n0

	for len(open) != 0 {
		// Find next node, which is the node with lowest estimated cost.
		// (Could have been speep up using tree map or map + appropriate list type.)
		var curr node
		for _, o := range open {
			if curr.Estimate == 0 || o.Estimate < curr.Estimate {
				curr = o
			}
		}
		delete(open, curr.Point)
		closed[curr.Point] = curr
		// Visit each neighbour, if it exist and is not already closed.
		for _, dxy := range [][]int{{-1, 0}, {1, 0}, {0, -1}, {0, 1}} {
			p := d5.Point{X: curr.X + dxy[0], Y: curr.Y + dxy[1]}
			c, ok := costs.Get(p)
			if _, cl := closed[p]; !ok || cl {
				continue
			}
			n := makeNode(p, curr.Cost+c, manhattanDistance(p, end))
			if p == end {
				return n.Cost
			}
			if o, ok := open[p]; ok && o.Cost <= n.Cost {
				continue
			}
			// Neighbour didn't exist or got a new lower cost using the current path.
			open[p] = n
		}
	}

	panic("Path not found")
}

// manhattanDistance Calculates the manhattan distance between input points.
func manhattanDistance(p1 d5.Point, p2 d5.Point) int {
	dx, _ := d5.AbsSgn(p1.X - p2.X)
	dy, _ := d5.AbsSgn(p1.Y - p2.Y)
	return dx + dy
}

// node is a point together with a cost and an estimate cost, used in path finding.
type node struct {
	d5.Point
	Cost     int
	Estimate int
}

// makeNode Creates a node based on the cost and a heuristic cost, where
// node.Estimate is cost+heuristics
func makeNode(point d5.Point, cost int, heuristics int) node {
	return node{point, cost, cost + heuristics}
}
