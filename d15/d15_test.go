package d15

import (
	"strings"
	"testing"
)

var example = `1163751742
1381373672
2136511328
3694931569
7463417111
1319128137
1359912421
3125421639
1293138521
2311944581`

// TestExample tests the solution against the example
func TestExample(t *testing.T) {
	input := strings.NewReader(example)
	sola, solb := Run(input)
	// Verify Solution A
	if a, ok := sola.(int); ok {
		if a != 40 {
			t.Errorf("Expected %d, got %d", 40, a)
		}
	} else {
		t.Errorf("Expected int, got %T", sola)
	}
	// Verify Solution B
	if b, ok := solb.(int); ok {
		if b != 315 {
			t.Errorf("Expected %d, got %d", 315, b)
		}
	} else {
		t.Errorf("Expected int, got %T", solb)
	}
}
