package d12

import (
	"aoc2021/d1"
	"io"
	"strings"
	"unicode"
)

// Run runs the program and returns the results
func Run(input io.Reader) (resA, resB interface{}) {
	// Create nodes linked with each other
	nodes := make(map[string]*node)
	for _, line := range d1.ReadLines(input) {
		link := strings.Split(line, "-")
		var n1, n2 *node
		var ok bool
		if n1, ok = nodes[link[0]]; !ok {
			n1 = makeNode(link[0])
			nodes[n1.Name] = n1
		}
		if n2, ok = nodes[link[1]]; !ok {
			n2 = makeNode(link[1])
			nodes[n2.Name] = n2
		}
		n1.addLink(n2)
		n2.addLink(n1)
	}

	// Count number of paths from start to end
	resA = paths(nil, false, nodes["start"], nodes["end"])
	resB = paths(nil, true, nodes["start"], nodes["end"])
	return
}

// paths sounts number of paths from start to end
func paths(visited map[string]struct{}, allowRevisit bool, start *node, end *node) int {
	if start.Name == end.Name {
		return 1
	}
	// Track lowercase visits
	if unicode.IsLower(rune(start.Name[0])) {
		visited = copyVisited(visited)
		visited[start.Name] = struct{}{}
	}
	// Create one path for each node not in visited list,
	// or with single revisit per path allowed
	count := 0
	for _, n2 := range start.Links {
		if _, ok := visited[n2.Name]; !ok || (allowRevisit && n2.Name != "start") {
			count += paths(visited, allowRevisit && !ok, n2, end)
		}
	}
	return count
}

// copyVisited returns a copy of the input map
func copyVisited(visited map[string]struct{}) map[string]struct{} {
	m := make(map[string]struct{})
	for k, v := range visited {
		m[k] = v
	}
	return m
}

// node is a named node with links to other nodes
type node struct {
	Name  string
	Links []*node
}

// addLink adds a link to another node
func (n *node) addLink(n2 *node) {
	n.Links = append(n.Links, n2)
}

// makeNode creates a named node
func makeNode(name string) *node {
	return &node{
		Name:  name,
		Links: make([]*node, 0),
	}
}
