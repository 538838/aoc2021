package d12

import (
	"strings"
	"testing"
)

var example = `fs-end
he-DX
fs-he
start-DX
pj-DX
end-zg
zg-sl
zg-pj
pj-he
RW-he
fs-DX
pj-RW
zg-RW
start-pj
he-WI
zg-he
pj-fs
start-RW`

// TestExample tests the solution against the example
func TestExample(t *testing.T) {
	input := strings.NewReader(example)
	sola, solb := Run(input)
	// Verify Solution A
	if a, ok := sola.(int); ok {
		if a != 226 {
			t.Errorf("Expected %d, got %d", 226, a)
		}
	} else {
		t.Errorf("Expected int, got %T", sola)
	}
	// Verify Solution B
	if b, ok := solb.(int); ok {
		if b != 3509 {
			t.Errorf("Expected %d, got %d", 3509, b)
		}
	} else {
		t.Errorf("Expected int, got %T", solb)
	}
}
