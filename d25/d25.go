package d25

import (
	"aoc2021/d1"
	"aoc2021/d9"
	"io"
)

// sea cucumbers direction is either east or south
type direction = int

const (
	east  direction = 2
	south direction = 4
)

// Run runs the program and returns the results
func Run(input io.Reader) (resA, resB interface{}) {
	// Parse input to a value map
	var vals [][]int
	for _, l := range d1.ReadLines(input) {
		v := make([]int, len(l))
		for i, r := range l {
			if r == '>' {
				v[i] = east
			} else if r == 'v' {
				v[i] = south
			}
		}
		vals = append(vals, v)
	}
	vm := d9.MakeValueMap(vals)

	// Step east and south until steady state
	steps := 0
	for c2, c4 := true, true; c2 || c4; {
		c2 = step(vm, east)
		c4 = step(vm, south)
		steps++
	}
	resA = steps
	resB = "<Press remote start>"
	return
}

// step moves all values in input value map matching input val in the val direction, if possible
func step(vm *d9.ValueMap, val direction) bool {
	// Pre-calculate x+1%vm.DimX and y+1%vm.DimY
	x2 := intsToN(vm.DimX)
	y2 := intsToN(vm.DimY)
	if val == east {
		x2 = append(x2[1:], 0)
	} else {
		y2 = append(y2[1:], 0)
	}
	// Move all values that can be moved, and mark changed values
	for y := 0; y < vm.DimY; y++ {
		for x := 0; x < vm.DimX; x++ {
			if vm.Val[y][x] == val && vm.Val[y2[y]][x2[x]] == 0 {
				vm.Val[y2[y]][x2[x]] = val + 1
				vm.Val[y][x] = 1
			}
		}
	}
	// Removed change marks and return true if any value was changed
	change := false
	for y := 0; y < vm.DimY; y++ {
		for x := 0; x < vm.DimX; x++ {
			if vm.Val[y][x]%2 == 1 {
				vm.Val[y][x]--
				change = true
			}
		}
	}
	return change
}

// intsToN returns a slice with int = [0; n)
func intsToN(n int) []int {
	s := make([]int, n)
	for i := 0; i < n; i++ {
		s[i] = i
	}
	return s
}
