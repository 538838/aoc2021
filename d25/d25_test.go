package d25

import (
	"strings"
	"testing"
)

var example = `v...>>.vv>
.vv>>.vv..
>>.>v>...v
>>v>>.>.v.
v>v.vv.v..
>.>>..v...
.vv..>.>v.
v.v..>>v.v
....v..v.>`

// TestExample tests the solution against the example
func TestExample(t *testing.T) {
	input := strings.NewReader(example)
	sola, _ := Run(input)
	// Verify Solution A
	if a, ok := sola.(int); ok {
		if a != 58 {
			t.Errorf("Expected %d, got %d", 58, a)
		}
	} else {
		t.Errorf("Expected int, got %T", sola)
	}
}
