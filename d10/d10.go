package d10

import (
	"aoc2021/d1"
	"io"
	"sort"
)

// Run runs the program and returns the results
func Run(input io.Reader) (resA, resB interface{}) {
	closing := map[rune]rune{')': '(', ']': '[', '}': '{', '>': '<'}
	corruptPoints := map[rune]int{')': 3, ']': 57, '}': 1197, '>': 25137}
	completePoints := map[rune]int{'(': 1, '[': 2, '{': 3, '<': 4}

	corruptScore := 0
	var completeScores []int
outer:
	for _, l := range d1.ReadLines(input) {
		var chunk []rune
		// Find unclosed chunk, or calculate score for corrupted lineS
		for _, r := range l {
			c, ok := closing[r]
			if !ok {
				chunk = append(chunk, r)
			} else if len(chunk) != 0 && chunk[len(chunk)-1] == c {
				chunk = chunk[:len(chunk)-1]
			} else {
				corruptScore += corruptPoints[r]
				continue outer
			}
		}
		// Complete chunk and calculate score
		sc := 0
		for i := len(chunk) - 1; i >= 0; i-- {
			sc = sc*5 + completePoints[chunk[i]]
		}
		completeScores = append(completeScores, sc)
	}

	resA = corruptScore
	sort.Ints(completeScores)
	resB = completeScores[len(completeScores)/2]
	return
}
