package d2

import (
	"strings"
	"testing"
)

var example = `forward 5
down 5
forward 8
up 3
down 8
forward 2`

// TestExample tests the solution against the example
func TestExample(t *testing.T) {
	input := strings.NewReader(example)
	sola, solb := Run(input)
	// Verify Solution A
	if a, ok := sola.(int); ok {
		if a != 150 {
			t.Errorf("Expected %d, got %d", 150, a)
		}
	} else {
		t.Errorf("Expected int, got %T", sola)
	}
	// Verify Solution B
	if b, ok := solb.(int); ok {
		if b != 900 {
			t.Errorf("Expected %d, got %d", 900, b)
		}
	} else {
		t.Errorf("Expected int, got %T", solb)
	}
}
