package d2

import (
	"aoc2021/d1"
	"io"
	"strconv"
	"strings"
)

// Run runs the program and returns the results
func Run(input io.Reader) (resA, resB interface{}) {
	dirs := splitLines(d1.ReadLines(input))

	posA := []int{0, 0}
	posB := []int{0, 0}
	aimB := 0
	for _, d := range dirs {
		i := MustAtoi(d[1])
		switch d[0] {
		case "forward":
			posA[0] += i
			posB[0] += i
			posB[1] += aimB * i
		case "down":
			posA[1] += i
			aimB += i
		case "up":
			posA[1] -= i
			aimB -= i
		default:
			panic("unknown direction " + d[0])
		}
	}
	resA = posA[0] * posA[1]
	resB = posB[0] * posB[1]

	return resA, resB
}

// MustAtoi converts string to int, or panics
func MustAtoi(s string) int {
	i, err := strconv.Atoi(s)
	if err != nil {
		panic(err)
	}
	return i
}

// splitLines splits each input line by space
func splitLines(str []string) [][]string {
	res := make([][]string, len(str))
	for i, s := range str {
		res[i] = strings.Split(s, " ")
	}
	return res
}
