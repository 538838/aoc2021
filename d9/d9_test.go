package d9

import (
	"strings"
	"testing"
)

var example = `2199943210
3987894921
9856789892
8767896789
9899965678`

// TestExample tests the solution against the example
func TestExample(t *testing.T) {
	input := strings.NewReader(example)
	sola, solb := Run(input)
	// Verify Solution A
	if a, ok := sola.(int); ok {
		if a != 15 {
			t.Errorf("Expected %d, got %d", 15, a)
		}
	} else {
		t.Errorf("Expected int, got %T", sola)
	}
	// Verify Solution B
	if b, ok := solb.(int); ok {
		if b != 1134 {
			t.Errorf("Expected %d, got %d", 1134, b)
		}
	} else {
		t.Errorf("Expected int, got %T", solb)
	}
}
