package d9

import (
	"aoc2021/d1"
	"aoc2021/d5"
	"io"
	"sort"
	"strconv"
)

// Run runs the program and returns the results
func Run(input io.Reader) (resA, resB interface{}) {
	// Parse input to map
	var vals [][]int
	for _, l := range d1.ReadLines(input) {
		vals = append(vals, ParseDigits(l))
	}
	vm := MakeValueMap(vals)

	// Find all valleys and count number of points leading to each.
	// Alternative (and probably better) solution would be to find valleys and
	// explore all positions around it (and marking visited positions).
	valleys := make(map[d5.Point]int)
	for y := 0; y < vm.DimY; y++ {
		for x := 0; x < vm.DimX; x++ {
			p := d5.Point{X: x, Y: y}
			if val, _ := vm.Get(p); val == 9 {
				continue
			}
			min := vm.findLocalMin(p)
			valleys[min]++
		}
	}

	// Get risk and size of valleys, and calculate results
	i := 0
	risk := 0
	sizes := make([]int, len(valleys))
	for p, s := range valleys {
		v, _ := vm.Get(p)
		risk += 1 + v
		sizes[i] = s
		i++
	}
	resA = risk
	sort.Ints(sizes)
	resB = sizes[len(sizes)-1] * sizes[len(sizes)-2] * sizes[len(sizes)-3]

	return
}

// ParseDigits parses a string number into a slice with the digits
func ParseDigits(str string) []int {
	var res []int
	for _, s := range str {
		i, err := strconv.Atoi(string(s))
		if err != nil {
			panic(err)
		}
		res = append(res, i)
	}
	return res
}

// ValueMap is a 2-D map of values
type ValueMap struct {
	Val  [][]int
	DimX int
	DimY int
}

// MakeValueMap creates a valueMap from input 2-D slice
func MakeValueMap(values [][]int) *ValueMap {
	vm := ValueMap{values, 0, len(values)}
	// Verify input first
	if vm.DimY == 0 {
		panic("Input can not be of 0 size")
	}
	vm.DimX = len(values[0])
	for _, yy := range values {
		if len(yy) != vm.DimX {
			panic("Input must be rectangular")
		}
	}
	return &vm
}

// Get returns the value for input point
func (m *ValueMap) Get(p d5.Point) (int, bool) {
	if p.X < 0 || p.Y < 0 || p.X >= m.DimX || p.Y >= m.DimY {
		return 0, false
	}
	return m.Val[p.Y][p.X], true
}

// findLocalMin finds local minimum, starting from given point
func (m *ValueMap) findLocalMin(p d5.Point) d5.Point {
outer:
	for {
		val, ok := m.Get(p)
		if !ok {
			panic("Not a valid point")
		}
		for _, d := range [][]int{{-1, 0}, {1, 0}, {0, -1}, {0, 1}} {
			p2 := d5.Point{X: p.X + d[0], Y: p.Y + d[1]}
			if v, ok := m.Get(p2); ok && v < val {
				p = p2
				continue outer
			}
		}
		return p
	}
}
