package d6

import (
	"aoc2021/d1"
	"io"
	"io/ioutil"
	"reflect"
	"strings"
)

// Run runs the program and returns the results
func Run(input io.Reader) (resA, resB interface{}) {
	// Assume 64 bits to avoid overflow (not a good solution generally, but it's enough for this use case).
	var i int
	if reflect.TypeOf(i).Size() < 8 {
		panic("Using int and assuming 64 bits")
	}

	// Read input and create array of fishes, grouped by "counter"
	s := strings.Split(MustReadAll(input), ",")
	var fishes [9]int
	for _, f := range d1.ParseInts(s, 10) {
		fishes[f]++
	}

	// Iterate 80/256 days
	for d := 1; d <= 256; d++ {
		f0 := fishes[0]
		for i := 0; i < len(fishes)-1; i++ {
			fishes[i] = fishes[i+1]
		}
		fishes[len(fishes)-1] = f0
		fishes[len(fishes)-3] += f0
		// A
		if d == 80 {
			resA = IntSum(fishes[:])
		}
	}
	// B
	resB = IntSum(fishes[:])

	return
}

// IntSum calculates the sum of the ints in the input slice
func IntSum(arr []int) int {
	sum := 0
	for _, i := range arr {
		sum += i
	}
	return sum
}

// MustReadAll reads all bytes from input reader and returns it as a string,
// or panics on error.
// The string is trimmed from leading and trailing whitespace before it's returned.
func MustReadAll(r io.Reader) string {
	b, err := ioutil.ReadAll(r)
	if err != nil {
		panic(err)
	}
	return strings.TrimSpace(string(b))
}
