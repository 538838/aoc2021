package d6

import (
	"strings"
	"testing"
)

var example = `3,4,3,1,2`

// TestExample tests the solution against the example
func TestExample(t *testing.T) {
	input := strings.NewReader(example)
	sola, solb := Run(input)
	// Verify Solution A
	if a, ok := sola.(int); ok {
		if a != 5934 {
			t.Errorf("Expected %d, got %d", 5934, a)
		}
	} else {
		t.Errorf("Expected int, got %T", sola)
	}
	// Verify Solution B
	if b, ok := solb.(int); ok {
		if b != 26984457539 {
			t.Errorf("Expected %d, got %d", 26984457539, b)
		}
	} else {
		t.Errorf("Expected int, got %T", solb)
	}
}
