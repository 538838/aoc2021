package d3

import (
	"strings"
	"testing"
)

var example = `00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010`

// TestExample tests the solution against the example
func TestExample(t *testing.T) {
	input := strings.NewReader(example)
	sola, solb := Run(input)
	// Verify Solution A
	if a, ok := sola.(int); ok {
		if a != 198 {
			t.Errorf("Expected %d, got %d", 198, a)
		}
	} else {
		t.Errorf("Expected int, got %T", sola)
	}
	// Verify Solution B
	if b, ok := solb.(int); ok {
		if b != 230 {
			t.Errorf("Expected %d, got %d", 230, b)
		}
	} else {
		t.Errorf("Expected int, got %T", solb)
	}
}
