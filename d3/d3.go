package d3

import (
	"aoc2021/d1"
	"fmt"
	"io"
)

// Run runs the program and returns the results
func Run(input io.Reader) (resA, resB interface{}) {
	lines := d1.ReadLines(input)
	nums := d1.ParseInts(lines, 2)
	bits := len(lines[0])

	// A
	gamma := 0
	epsilon := 0
	for b := 0; b < bits; b++ {
		m, l := mostLeastCommon(nums, b)
		gamma |= (m << b)
		epsilon |= (l << b)
	}
	resA = gamma * epsilon

	// B
	o2 := make([]int, len(nums))
	copy(o2, nums)
	for b := bits - 1; b >= 0 && len(o2) > 1; b-- {
		m, _ := mostLeastCommon(o2, b)
		o2 = filterInts(o2, func(n int) bool {
			return (n>>b)&1 == m
		})
	}
	co2 := make([]int, len(nums))
	copy(co2, nums)
	for b := bits - 1; b >= 0 && len(co2) > 1; b-- {
		_, l := mostLeastCommon(co2, b)
		co2 = filterInts(co2, func(n int) bool {
			return (n>>b)&1 == l
		})
	}
	if len(o2) != 1 || len(co2) != 1 {
		panic(fmt.Sprintf("Expected length 1, got %d, %d", len(o2), len(co2)))
	}
	resB = o2[0] * co2[0]

	return
}

// filterInts filters input array by calling the test method for each number
func filterInts(nums []int, test func(int) bool) []int {
	var res []int
	for _, n := range nums {
		if test(n) {
			res = append(res, n)
		}
	}
	return res
}

// mostLeastCommon returns most and least common bit value (1 or 0), in the input numbers
// considering only the bit at given position
func mostLeastCommon(nums []int, bit int) (int, int) {
	sgn := 0
	for _, n := range nums {
		if n&(1<<bit) != 0 {
			sgn++
		} else {
			sgn--
		}
	}
	if sgn >= 0 {
		return 1, 0
	}
	return 0, 1
}
