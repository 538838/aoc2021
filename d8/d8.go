package d8

import (
	"aoc2021/d1"
	"aoc2021/d2"
	"io"
	"sort"
	"strings"
)

// Run runs the program and returns the results
func Run(input io.Reader) (resA, resB interface{}) {
	lines := d1.ReadLines(input)

	count1478 := 0
	sum := 0
	for _, l := range lines {
		s := strings.Split(l, "|")
		p := strings.Split(strings.TrimSpace(s[0]), " ")
		v := strings.Split(strings.TrimSpace(s[1]), " ")
		num := getNumber(p, v)
		sum += d2.MustAtoi(num)
		count1478 += len(num) - len(subtractRunes(num, "1478"))
	}
	resA = count1478
	resB = sum

	return
}

// getNumber returns the number based on input patterns and digits.
func getNumber(patterns, digits []string) string {
	nums := make(map[string]rune)

	// Find simple numbers and useful segment groups
	var segcf string
	var segcbdf string
	for _, p := range patterns {
		p = sortRunes(p)
		switch len(p) {
		case 2:
			nums[p] = '1'
			segcf = p
		case 3:
			nums[p] = '7'
		case 4:
			nums[p] = '4'
			segcbdf = p
		case 7:
			nums[p] = '8'
		}
	}
	segbd := subtractRunes(segcbdf, segcf)

	// Find the other numbers using length and segment groups
	for _, p := range patterns {
		p = sortRunes(p)
		if len(p) == 5 {
			if containsRunes(p, segcf) {
				nums[p] = '3'
			} else if containsRunes(p, segbd) {
				nums[p] = '5'
			} else {
				nums[p] = '2'
			}
		} else if len(p) == 6 {
			if containsRunes(p, segcf) {
				if containsRunes(p, segbd) {
					nums[p] = '9'
				} else {
					nums[p] = '0'
				}
			} else {
				nums[p] = '6'
			}
		}
	}

	// Parse input digits to a string number
	res := ""
	for _, v := range digits {
		v = sortRunes(v)
		res += string(nums[v])
	}
	return res
}

// sortRunes sorts input string by runes.
func sortRunes(s string) string {
	r := []rune(s)
	sort.Slice(r, func(i, j int) bool {
		return r[i] < r[j]
	})
	return string(r)
}

// subtractRunes subtract all runes in input runes from input str.
func subtractRunes(str, runes string) string {
	res := []rune(str)
	for _, r := range runes {
		for i := 0; i < len(res); i++ {
			if res[i] == r {
				res[i] = res[len(res)-1]
				res = res[:len(res)-1]
				i--
			}
		}
	}
	return string(res)
}

// containsRunes returns true if input string contains all runes in input runes.
func containsRunes(str, runes string) bool {
	for _, r := range runes {
		found := false
		for _, s := range str {
			if s == r {
				found = true
				break
			}
		}
		if !found {
			return false
		}
	}
	return true
}
