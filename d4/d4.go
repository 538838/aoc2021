package d4

import (
	"aoc2021/d1"
	"io"
	"regexp"
	"strings"
)

// Run runs the program and returns the results
func Run(input io.Reader) (resA, resB interface{}) {
	lines := d1.ReadLines(input)
	draw := d1.ParseInts(strings.Split(lines[0], ","), 10)

	// Parse input to board maps
	var boards []*board
	var curr [][]int
	for _, line := range lines[2:] {
		line := truncWhiteSpace(line)
		if len(line) == 0 {
			boards = append(boards, makeBoard(curr))
			curr = nil
		} else {
			nums := d1.ParseInts(strings.Split(line, " "), 10)
			curr = append(curr, nums)
		}
	}
	boards = append(boards, makeBoard(curr))

	// Draw one at the time, calculate score and remove boards that have won
outer:
	for _, d := range draw {
		for i := 0; i < len(boards); i++ {
			if score := boards[i].mark(d); score != -1 {
				boards[i] = boards[len(boards)-1]
				boards = boards[:len(boards)-1]
				i--
				if resA == nil {
					resA = score
				}
				if len(boards) == 0 {
					resB = score
					break outer
				}
			}
		}
	}

	return
}

// board is a bingo board
type board struct {
	numbers map[int][2]int
	score   int
	rows    []int
	cols    []int
}

// makeBoard creates a board from an int matrix
func makeBoard(m [][]int) *board {
	b := board{
		rows: make([]int, len(m)),
		cols: make([]int, len(m)),
	}
	b.numbers = make(map[int][2]int)
	for ri, r := range m {
		for ci, c := range r {
			b.numbers[c] = [2]int{ri, ci}
			b.score += c
		}
	}
	return &b
}

// mark a number on the board.
// Returns the score if the draw resulted in bingo, otherwise returns -1
func (b *board) mark(num int) int {
	pos, ok := b.numbers[num]
	if !ok {
		return -1
	}
	b.rows[pos[0]]++
	b.cols[pos[1]]++
	b.score -= num
	if b.rows[pos[0]] == len(b.rows) || b.cols[pos[1]] == len(b.cols) {
		return b.score * num
	}
	return -1
}

var whiteSpace = regexp.MustCompile(`\s+`)

// truncWhiteSpace replaces all white space with a single space and
// removes leading and trailing white space
func truncWhiteSpace(s string) string {
	return whiteSpace.ReplaceAllString(strings.TrimSpace(s), " ")
}
