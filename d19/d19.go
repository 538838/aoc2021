package d19

import (
	"aoc2021/d1"
	"aoc2021/d5"
	"io"
	"strings"
)

// Run runs the program and returns the results
func Run(input io.Reader) (resA, resB interface{}) {
	var scanners []scanner
	// parse input to scanner array
	beacons := make(map[Point]struct{})
	for _, l := range d1.ReadLines(input) {
		if strings.HasPrefix(l, "--- scanner") {
			continue
		}
		if l == "" {
			scanners = append(scanners, makeScanner(beacons))
			beacons = make(map[Point]struct{})
		} else {
			xyz := d1.ParseInts(strings.Split(l, ","), 10)
			beacons[Point{xyz[0], xyz[1], xyz[2]}] = struct{}{}
		}
	}
	scanners = append(scanners, makeScanner(beacons))

	// Try to find solutions for all scanners, using first scanner as origin and correct rotation.
	solved := []scanner{scanners[0]}
	open := scanners[1:]
	for s := 0; s < len(solved); s++ {
		for o := 0; o < len(open); o++ {
			if s2rt, ok := solved[s].findOverlap(open[o]); ok {
				solved = append(solved, s2rt)
				open[o] = open[len(open)-1]
				open = open[:len(open)-1]
				o--
			}
		}
	}
	if len(open) != 0 {
		panic("Could not find solutions for all scanners")
	}

	// Count number of unique beacons
	beacons = make(map[Point]struct{})
	for _, s := range solved {
		for b := range s.Beacons {
			beacons[b] = struct{}{}
		}
	}
	resA = len(beacons)
	// Find maximum distance between any two scanners
	maxDist := 0
	for i, s1 := range solved[:len(solved)-1] {
		for _, s2 := range solved[i+1:] {
			if d := s1.Position.manhattanDistance(s2.Position); d > maxDist {
				maxDist = d
			}
		}
	}
	resB = maxDist

	return
}

// scanner is a scanner position and the absolute values of the beacons seen by the scanner
type scanner struct {
	Position Point
	Beacons  map[Point]struct{}
}

// findOverlap tries to find a rotation+translation of input scanner so that it
// overlaps this scanner. Returns true and a rotated and translated scanner if found.
func (s scanner) findOverlap(s2 scanner) (scanner, bool) {
	// Try every rotation (but don't do any transformation when rotation is 0)
	for r := 0; r < 24; r++ {
		s2r := s2
		if r != 0 {
			s2r = s2.rotateByIndex(r)
		}
		// Try translate every beacons in rotated s2 to every beacon in s1, skipping the
		// last 11, because the overlap is either found before this or not at all.
		left := len(s.Beacons)
		for b1 := range s.Beacons {
			for b2 := range s2r.Beacons {
				t := []int{b1.X - b2.X, b1.Y - b2.Y, b1.Z - b2.Z}
				s2rt := s2r.translate(t)
				if s.overlaps(s2rt) {
					return s2rt, true
				}
			}
			if left--; left < 12 {
				break
			}
		}
	}
	return scanner{}, false
}

// makeScanner returns a scanner with input beacons as beacons
func makeScanner(beacons map[Point]struct{}) scanner {
	return scanner{Point{}, beacons}
}

// returns true if this scanner can be determined to overlap input scanner
func (s scanner) overlaps(s2 scanner) bool {
	count := 0
	for b := range s.Beacons {
		if _, ok := s2.Beacons[b]; ok {
			count++
		}
	}
	return count >= 12
}

// rotateByIndex rotates all scanner coordinates by one of the 24 [0:23] 3D-rotations around origin
func (s scanner) rotateByIndex(index int) scanner {
	s2 := scanner{Beacons: make(map[Point]struct{})}
	for b := range s.Beacons {
		s2.Beacons[b.rotateByIndex(index)] = struct{}{}
	}
	return s2
}

// translate translate all scanner coordinates using the input vector of length 3
func (s scanner) translate(v []int) scanner {
	s2 := scanner{Beacons: make(map[Point]struct{})}
	s2.Position = s2.Position.translate(v)
	for b := range s.Beacons {
		s2.Beacons[b.translate(v)] = struct{}{}
	}
	return s2
}

// Point is a Point in 3D space, with int X, Y Z coordinates.
type Point struct {
	X int
	Y int
	Z int
}

// manhattanDistance returns the manhattain distance to the input point
func (p Point) manhattanDistance(p2 Point) int {
	d1, _ := d5.AbsSgn(p.X - p2.X)
	d2, _ := d5.AbsSgn(p.Y - p2.Y)
	d3, _ := d5.AbsSgn(p.Z - p2.Z)
	return d1 + d2 + d3
}

// translate translate the point using the input vector of length 3
func (p Point) translate(v []int) Point {
	if len(v) != 3 {
		panic("Translation vector must have size 3")
	}
	return Point{p.X + v[0], p.Y + v[1], p.Z + v[2]}
}

// tranform tranforms the point using the 3x3 input matrix
func (p Point) tranform(m [][]int) Point {
	if len(m) != 3 || len(m[0]) != 3 || len(m[1]) != 3 || len(m[2]) != 3 {
		panic("Transformation matrix must have size 3x3")
	}
	return Point{
		X: p.X*m[0][0] + p.Y*m[0][1] + p.Z*m[0][2],
		Y: p.X*m[1][0] + p.Y*m[1][1] + p.Z*m[1][2],
		Z: p.X*m[2][0] + p.Y*m[2][1] + p.Z*m[2][2],
	}
}

// rotateByIndex transforms the point by one of the 24 [0:23] 3D-rotations around origin
func (p Point) rotateByIndex(index int) Point {
	if index < 0 || index > 23 {
		panic("Index must be within  [0;23]")
	}
	// These should prehaps be generated at program start, but for simplicity
	// they are just listed here and used by index.
	rot := [][][]int{
		{{1, 0, 0}, {0, 1, 0}, {0, 0, 1}},
		{{1, 0, 0}, {0, 0, -1}, {0, 1, 0}},
		{{1, 0, 0}, {0, -1, 0}, {0, 0, -1}},
		{{1, 0, 0}, {0, 0, 1}, {0, -1, 0}},
		{{0, -1, 0}, {1, 0, 0}, {0, 0, 1}},
		{{0, 0, 1}, {1, 0, 0}, {0, 1, 0}},
		{{0, 1, 0}, {1, 0, 0}, {0, 0, -1}},
		{{0, 0, -1}, {1, 0, 0}, {0, -1, 0}},
		{{-1, 0, 0}, {0, -1, 0}, {0, 0, 1}},
		{{-1, 0, 0}, {0, 0, -1}, {0, -1, 0}},
		{{-1, 0, 0}, {0, 1, 0}, {0, 0, -1}},
		{{-1, 0, 0}, {0, 0, 1}, {0, 1, 0}},
		{{0, 1, 0}, {-1, 0, 0}, {0, 0, 1}},
		{{0, 0, 1}, {-1, 0, 0}, {0, -1, 0}},
		{{0, -1, 0}, {-1, 0, 0}, {0, 0, -1}},
		{{0, 0, -1}, {-1, 0, 0}, {0, 1, 0}},
		{{0, 0, -1}, {0, 1, 0}, {1, 0, 0}},
		{{0, 1, 0}, {0, 0, 1}, {1, 0, 0}},
		{{0, 0, 1}, {0, -1, 0}, {1, 0, 0}},
		{{0, -1, 0}, {0, 0, -1}, {1, 0, 0}},
		{{0, 0, -1}, {0, -1, 0}, {-1, 0, 0}},
		{{0, -1, 0}, {0, 0, 1}, {-1, 0, 0}},
		{{0, 0, 1}, {0, 1, 0}, {-1, 0, 0}},
		{{0, 1, 0}, {0, 0, -1}, {-1, 0, 0}},
	}

	return p.tranform(rot[index])
}
