package d1

import (
	"bufio"
	"io"
	"strconv"
)

// Run runs the program and returns the results
func Run(input io.Reader) (resA, resB interface{}) {
	ints := ParseInts(ReadLines(input), 10)

	inca := 0
	for i := 0; i < len(ints)-1; i++ {
		if ints[i] < ints[i+1] {
			inca++
		}
	}

	incb := 0
	for i := 0; i < len(ints)-3; i++ {
		// i0+i1+i2 < i1+i2+i3 <=> i0 < i3
		if ints[i] < ints[i+3] {
			incb++
		}
	}

	return inca, incb
}

// ParseInts parses a string array into an int array.
// Panics on error.
func ParseInts(str []string, base int) []int {
	res := make([]int, len(str))
	for i, s := range str {
		r, err := strconv.ParseInt(s, base, 32)
		if err != nil {
			panic(err)
		}
		res[i] = int(r)
	}
	return res
}

// ReadLines reads splits text from a reader into a string array.
// Panics on error.
func ReadLines(reader io.Reader) []string {
	var res []string
	sc := bufio.NewScanner(reader)
	for sc.Scan() {
		res = append(res, sc.Text())
	}
	if sc.Err() != nil {
		panic(sc.Err())
	}
	return res
}
