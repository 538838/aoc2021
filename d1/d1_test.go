package d1

import (
	"strings"
	"testing"
)

var example = `199
200
208
210
200
207
240
269
260
263`

// TestExample tests the solution against the example
func TestExample(t *testing.T) {
	input := strings.NewReader(example)
	sola, solb := Run(input)
	// Verify Solution A
	if a, ok := sola.(int); ok {
		if a != 7 {
			t.Errorf("Expected %d, got %d", 7, a)
		}
	} else {
		t.Errorf("Expected int, got %T", sola)
	}
	// Verify Solution B
	if b, ok := solb.(int); ok {
		if b != 5 {
			t.Errorf("Expected %d, got %d", 5, b)
		}
	} else {
		t.Errorf("Expected int, got %T", solb)
	}
}
