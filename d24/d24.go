package d24

import (
	"aoc2021/d1"
	"aoc2021/d14"
	"aoc2021/d2"
	"fmt"
	"io"
	"strings"
)

// This solution depends on many details manually found in the puzzle input.
// For example that z register is the only register transfered to next digit input,
// and that a input z max is 26 * (max z out + 1).
// These numbers and registers could be different for different puzzle inputs.

// Run runs the program and returns the results
func Run(input io.Reader) (resA, resB interface{}) {
	// Assume int64, otherwise the result will overflow
	d14.AssertInt64()

	// add breakpoints
	lines := d1.ReadLines(input)
	prgm := compile(lines)
	var bp []int
	for i := 0; i < len(lines); i++ {
		if strings.HasPrefix(lines[i], "inp") {
			bp = append(bp, i)
		}
	}
	parts := len(bp)
	bp = append(bp, len(lines))

	// Only the Z register is transfered between the program parts (each part
	// containing a single inp).
	// Find valid input z, based on valid output z
	outputZ := make([]map[int]struct{}, parts)
	outputZ[parts-1] = map[int]struct{}{0: {}}
	for i := parts - 1; i >= 1; i-- {
		r := validZ(outputZ[i], prgm[bp[i]:bp[i+1]])
		outputZ[i-1] = r
	}

	// Based on valid Z, find max and min input for each program part
	maxSerno := 0
	minSerno := 0
	var d, z1, z2 int
	for i := 0; i < parts; i++ {
		d, z1 = maxIn(outputZ[i], z1, prgm[bp[i]:bp[i+1]])
		maxSerno = maxSerno*10 + d
		d, z2 = minIn(outputZ[i], z2, prgm[bp[i]:bp[i+1]])
		minSerno = minSerno*10 + d
	}
	mustVerifySerno(maxSerno, prgm)
	mustVerifySerno(minSerno, prgm)

	resA = maxSerno
	resB = minSerno
	return
}

// mustVerifySerno verifies the input serial number with the input program
// and panics if the serial number is not valid
func mustVerifySerno(serno int, prgm []instruction) {
	a := alu{}
	for serno != 0 {
		a.inp = append(a.inp, serno%10)
		serno /= 10
	}
	a.run(prgm)
	if a.reg[3] != 0 {
		panic("Invalid serial number")
	}
}

// maxIn find the maximum valid input for the input program and valid resulting z
// and returns the input and the resulting z
func maxIn(outZ map[int]struct{}, zIn int, prgm []instruction) (int, int) {
	for i := 9; i >= 1; i-- {
		a := alu{}
		a.reg[3] = zIn
		a.inp = []int{i}
		a.run(prgm)
		if _, ok := outZ[a.reg[3]]; ok {
			return i, a.reg[3]
		}
	}
	panic("Solution not found")
}

// minIn find the minimum valid input for the input program and valid resulting z
// and returns the input and the resulting z
func minIn(outZ map[int]struct{}, z int, prgm []instruction) (int, int) {
	for i := 1; i <= 9; i++ {
		a := alu{}
		a.reg[3] = z
		a.inp = []int{i}
		a.run(prgm)
		if _, ok := outZ[a.reg[3]]; ok {
			return i, a.reg[3]
		}
	}
	panic("Solution not found")
}

// validZ find the valid input z for the input program and valid resulting z
func validZ(out map[int]struct{}, prgm []instruction) map[int]struct{} {
	in := make(map[int]struct{})
	// Looking at the code reveals that max input is 26 * (max out + 1)
	max := 0
	for o := range out {
		if o > max {
			max = o
		}
	}
	max = 26 * (max + 1)

	// Find input z that gives a valid output z for any input [1;9]
	for z := 0; z <= max; z++ {
		for i := 1; i <= 9; i++ {
			a := alu{}
			a.reg[3] = z
			a.inp = []int{i}
			a.run(prgm)
			if _, ok := out[a.reg[3]]; ok {
				in[z] = struct{}{}
				break
			}
		}
	}
	return in
}

// compile parses the input program to instructions
func compile(code []string) (prgm []instruction) {
	regs := map[string]int{"w": 0, "x": 1, "y": 2, "z": 3}
	for _, l := range code {
		op := strings.Split(l, " ")
		i := instruction{0, regs[op[1]], 0}
		switch op[0] {
		case "inp":
			i[0] = inp
		case "add":
			i[0] = add
		case "mul":
			i[0] = mul
		case "div":
			i[0] = div
		case "mod":
			i[0] = mod
		case "eql":
			i[0] = eql
		default:
			panic(fmt.Sprintf("Unknown operation '%s'", l))
		}
		if op[0] != "inp" {
			if r, ok := regs[op[2]]; ok {
				i[2] = r
				i[0]++
			} else {
				i[2] = d2.MustAtoi(op[2])
			}
		}
		prgm = append(prgm, i)
	}

	return prgm
}

type instruction = [3]int // op, reg, value

// operations
const (
	inp = iota
	add
	addr
	mul
	mulr
	div
	divr
	mod
	modr
	eql
	eqlr
)

// alu is an arithmetic logic unit, with 4 registers and an input stack
type alu struct {
	reg [4]int // w, x, y, z
	inp []int  // input stack
}

// run the input program
func (a *alu) run(prgm []instruction) {
	for _, i := range prgm {
		switch i[0] {
		case inp:
			a.reg[i[1]] = a.inp[len(a.inp)-1]
			a.inp = a.inp[:len(a.inp)-1]
		case add:
			a.reg[i[1]] += i[2]
		case addr:
			a.reg[i[1]] += a.reg[i[2]]
		case mul:
			a.reg[i[1]] *= i[2]
		case mulr:
			a.reg[i[1]] *= a.reg[i[2]]
		case div:
			a.reg[i[1]] /= i[2]
		case divr:
			a.reg[i[1]] /= a.reg[i[2]]
		case mod:
			a.reg[i[1]] %= i[2]
		case modr:
			a.reg[i[1]] %= a.reg[i[2]]
		case eql:
			if a.reg[i[1]] == i[2] {
				a.reg[i[1]] = 1
			} else {
				a.reg[i[1]] = 0
			}
		case eqlr:
			if a.reg[i[1]] == a.reg[i[2]] {
				a.reg[i[1]] = 1
			} else {
				a.reg[i[1]] = 0
			}
		}
	}
}
