package d24

import (
	"strings"
	"testing"
)

var example = `inp w
mul x 0
add x z
mod x 26
div z 1
add x 11
eql x w
eql x 0
mul y 0
add y 25
mul y x
add y 1
mul z y
mul y 0
add y w
add y 10
mul y x
add z y
inp w
mul x 0
add x z
mod x 26
div z 26
add x -16
eql x w
eql x 0
mul y 0
add y 25
mul y x
add y 1
mul z y
mul y 0
add y w
add y 8
mul y x
add z y
inp w
mul x 0
add x z
mod x 26
div z 26
add x -2
eql x w
eql x 0
mul y 0
add y 25
mul y x
add y 1
mul z y
mul y 0
add y w
add y 15
mul y x
add z y`

// TestExample tests the solution against the last 3 input parts of the puzzle input.
func TestExample(t *testing.T) {
	input := strings.NewReader(example)
	sola, solb := Run(input)
	// Verify Solution A
	if a, ok := sola.(int); ok {
		if a != 928 {
			t.Errorf("Expected %d, got %d", 928, a)
		}
	} else {
		t.Errorf("Expected int, got %T", sola)
	}
	// Verify Solution B
	if b, ok := solb.(int); ok {
		if b != 117 {
			t.Errorf("Expected %d, got %d", 117, b)
		}
	} else {
		t.Errorf("Expected int, got %T", solb)
	}
}
