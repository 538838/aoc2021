package d21

import (
	"aoc2021/d1"
	"aoc2021/d14"
	"aoc2021/d16"
	"aoc2021/d2"
	"io"
)

// Run runs the program and returns the results
func Run(input io.Reader) (resA, resB interface{}) {
	d14.AssertInt64()
	// Parse input to initial state
	lines := d1.ReadLines(input)
	prefix := "Player # starting position: "
	p1 := d2.MustAtoi(lines[0][len(prefix):])
	p2 := d2.MustAtoi(lines[1][len(prefix):])
	var s0 state
	s0.pos = [2]int{p1, p2}

	// A
	d := &dice{}
	s := s0
	for {
		// Roll until a player was more than 1000 points
		ns := s.step(d.roll3())
		if ns.sum[s.nxt] >= 1000 {
			resA = s.sum[ns.nxt] * d.rolls
			break
		}
		s = ns
	}

	// B
	// Possible outcomes of three three side dices with the count of permutations for that outcome
	diracRolls := map[int]int{3: 1, 4: 3, 5: 6, 6: 7, 7: 6, 8: 3, 9: 1}
	states := map[state]int{s0: 1}
	wins := [2]int{0, 0}
	for len(states) > 0 {
		for s, c1 := range states {
			delete(states, s)
			for r, c2 := range diracRolls {
				// For each outcome count the number of times it can occur in total
				// and add to states, or if the player won add to win count
				ns := s.step(r)
				if ns.sum[s.nxt] >= 21 {
					wins[s.nxt] += c1 * c2
				} else {
					states[ns] += c1 * c2
				}
			}
		}
	}
	resB = d16.Maximum(wins[:])
	return
}

// state is the game state
type state struct {
	pos [2]int
	sum [2]int
	nxt int
}

// step takes int input number of steps for the current player
func (s state) step(steps int) state {
	ns := s
	ns.pos[s.nxt] = ((ns.pos[s.nxt] + steps - 1) % 10) + 1
	ns.sum[s.nxt] += ns.pos[s.nxt]
	ns.nxt = (s.nxt + 1) % 2
	return ns
}

// dice represents a 100 sided deterministic dice
type dice struct {
	val   int
	rolls int
}

// roll3 rolls the dice three times and returns the sum of the rolls
func (d *dice) roll3() int {
	s := 0
	for i := 0; i < 3; i++ {
		d.val = (d.val % 100) + 1
		s += d.val
	}
	d.rolls += 3
	return s
}
