package d21

import (
	"strings"
	"testing"
)

var example = `Player 1 starting position: 4
Player 2 starting position: 8`

// TestExample tests the solution against the example
func TestExample(t *testing.T) {
	input := strings.NewReader(example)
	sola, solb := Run(input)
	// Verify Solution A
	if a, ok := sola.(int); ok {
		if a != 739785 {
			t.Errorf("Expected %d, got %d", 739785, a)
		}
	} else {
		t.Errorf("Expected int, got %T", sola)
	}
	// Verify Solution B
	if b, ok := solb.(int); ok {
		if b != 444356092776315 {
			t.Errorf("Expected %d, got %d", 444356092776315, b)
		}
	} else {
		t.Errorf("Expected int, got %T", solb)
	}
}
