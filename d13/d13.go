package d13

import (
	"aoc2021/d1"
	"aoc2021/d2"
	"aoc2021/d5"
	"fmt"
	"io"
	"strings"
)

// Run runs the program and returns the results
func Run(input io.Reader) (resA, resB interface{}) {
	lines := d1.ReadLines(input)
	// Parse dots from input
	sm := MakeSparseMap()
	for i, line := range lines {
		if line == "" {
			lines = lines[i+1:]
			break
		}
		xy := d1.ParseInts(strings.Split(line, ","), 10)
		sm.Set(d5.Point{X: xy[0], Y: xy[1]}, 1)
	}
	// Parse folds (as points) from input
	var folds []d5.Point
	for _, line := range lines {
		if f := strings.Split(line, "="); f[0] == "fold along x" {
			folds = append(folds, d5.Point{X: d2.MustAtoi(f[1]), Y: 0})
		} else if f[0] == "fold along y" {
			folds = append(folds, d5.Point{X: 0, Y: d2.MustAtoi(f[1])})
		} else {
			panic(fmt.Sprintf("Not a valid fold: %s", line))
		}
	}

	// Fold one time for part A
	fold(sm, folds[0])
	resA = len(sm.Val)
	// Iterate over the rest of the folds and set result B as the string representation
	for _, f := range folds[1:] {
		fold(sm, f)
	}
	sm.UpdateSize()
	resB = sm.ToString(map[int]rune{0: '.', 1: '#'})

	return
}

// SparseMap is a sparse 2-D map of values. Only values != 0 are saved
type SparseMap struct {
	Val  map[d5.Point]int
	DimX [2]int
	DimY [2]int
}

// MakeSparseMap creates and returns a sparse map
func MakeSparseMap() *SparseMap {
	return &SparseMap{
		Val:  make(map[d5.Point]int),
		DimX: [2]int{0, 0},
		DimY: [2]int{0, 0},
	}
}

// Set sets the value at point p. If the value is 0 the point is removed.
func (m *SparseMap) Set(p d5.Point, i int) {
	if i == 0 {
		delete(m.Val, p)
	} else {
		m.Val[p] = i
	}
}

// UpdateSize calulates size based on minimum and maximum x and y
func (m *SparseMap) UpdateSize() {
	m.DimX = [2]int{0, 0}
	m.DimY = [2]int{0, 0}
	for p := range m.Val {
		if p.X < m.DimX[0] {
			m.DimX[0] = p.X
		}
		if p.X > m.DimX[1] {
			m.DimX[1] = p.X
		}
		if p.Y < m.DimY[0] {
			m.DimY[0] = p.Y
		}
		if p.Y > m.DimY[1] {
			m.DimY[1] = p.Y
		}
	}
}

// ToString creates a DimX * DimY string representation of the sparse map,
// printing each value according to the input rune map.
func (m *SparseMap) ToString(runes map[int]rune) string {
	var sb strings.Builder
	for y := m.DimY[0]; y <= m.DimY[1]; y++ {
		sb.WriteString("\n")
		for x := m.DimX[0]; x <= m.DimX[1]; x++ {
			v := m.Val[d5.Point{X: x, Y: y}]
			if r, ok := runes[v]; ok {
				sb.WriteRune(r)
			} else {
				sb.WriteRune(' ')
			}
		}
	}
	return sb.String()
}

// fold folds a sparse map at the input point
func fold(m *SparseMap, fold d5.Point) {
	for p := range m.Val {
		if p.X < fold.X || p.Y < fold.Y {
			continue
		}
		m.Set(p, 0)
		if fold.X != 0 && p.X > fold.X {
			p.X = 2*fold.X - p.X
			m.Set(p, 1)
		} else if fold.Y != 0 && p.Y > fold.Y {
			p.Y = 2*fold.Y - p.Y
			m.Set(p, 1)
		}
	}
}
