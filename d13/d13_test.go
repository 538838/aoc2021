package d13

import (
	"strings"
	"testing"
)

var example = `6,10
0,14
9,10
0,3
10,4
4,11
6,0
6,12
4,1
0,13
10,12
3,4
3,0
8,4
1,10
2,14
8,10
9,0

fold along y=7
fold along x=5`

var solutionB = `
#####
#...#
#...#
#...#
#####`

// TestExample tests the solution against the example
func TestExample(t *testing.T) {
	input := strings.NewReader(example)
	sola, solb := Run(input)
	// Verify Solution A
	if a, ok := sola.(int); ok {
		if a != 17 {
			t.Errorf("Expected %d, got %d", 17, a)
		}
	} else {
		t.Errorf("Expected int, got %T", sola)
	}
	// Verify Solution B
	if b, ok := solb.(string); ok {
		if b != solutionB {
			t.Errorf("Expected: \n%s\nGot:\n%s", solutionB, b)
		}
	} else {
		t.Errorf("Expected string, got %T", solb)
	}
}
