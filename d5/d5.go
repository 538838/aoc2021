package d5

import (
	"aoc2021/d1"
	"io"
	"strings"
)

// Run runs the program and returns the results
func Run(input io.Reader) (resA, resB interface{}) {
	strs := replaceString(d1.ReadLines(input), " -> ", ",")

	var linesA []line
	var linesB []line
	for _, l := range strs {
		p := d1.ParseInts(strings.Split(l, ","), 10)
		p1 := Point{p[0], p[1]}
		p2 := Point{p[2], p[3]}
		if p1.X == p2.X || p1.Y == p2.Y {
			linesA = append(linesA, line{p1, p2})
		}
		linesB = append(linesB, line{p1, p2})
	}

	resA = countOverlap(linesA)
	resB = countOverlap(linesB)

	return
}

// Point represents a two dimensional point.
type Point struct {
	X int
	Y int
}

// line represents a line between two points.
type line struct {
	P1 Point
	P2 Point
}

// points returns the points with integer coordinates along the line.
// For lines not horisontal, vertical or diagonal some rounding is made.
func (l *line) points() []Point {
	dx, kx := AbsSgn(l.P2.X - l.P1.X)
	dy, ky := AbsSgn(l.P2.Y - l.P1.Y)
	steps := 0
	if dx == 0 && dy == 0 {
		kx = 0
		ky = 0
	} else if dx >= dy {
		ky *= dy / dx
		steps = dx
	} else {
		kx *= dx / dy
		steps = dy
	}
	points := []Point{}
	for a := 0; a <= steps; a++ {
		x := l.P1.X + kx*a
		y := l.P1.Y + ky*a
		points = append(points, Point{x, y})
	}
	return points
}

// AbsSgn returns the absolute value and the sign of an int
func AbsSgn(val int) (abs, sgn int) {
	if val < 0 {
		return -val, -1
	}
	return val, 1
}

// countOverlap counts the number of overlaping points for an array of lines
func countOverlap(lines []line) int {
	points := make(map[Point]int)
	for _, l := range lines {
		for _, p := range l.points() {
			points[p]++
		}
	}
	cnt := 0
	for _, c := range points {
		if c >= 2 {
			cnt++
		}
	}
	return cnt
}

// replaceString replaces the first occurance of substr with repstr, for all
// strings in input array.
// Panics if the substring does not exist.
func replaceString(str []string, substr string, repstr string) []string {
	var res []string
	for _, s := range str {
		i := strings.Index(s, substr)
		if i == -1 {
			panic("Substring not found")
		}
		s := s[:i] + repstr + s[i+len(substr):]
		res = append(res, s)
	}
	return res
}
