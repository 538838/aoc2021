package d23

import (
	"strings"
	"testing"
)

var example = `#############
#...........#
###B#C#B#D###
  #A#D#C#A#
  #########`

// TestExample tests the solution against the example
func TestExample(t *testing.T) {
	input := strings.NewReader(example)
	sola, solb := Run(input)
	// Verify Solution A
	if a, ok := sola.(int); ok {
		if a != 12521 {
			t.Errorf("Expected %d, got %d", 12521, a)
		}
	} else {
		t.Errorf("Expected int, got %T", sola)
	}
	// Verify Solution B
	if b, ok := solb.(int); ok {
		if b != 44169 {
			t.Errorf("Expected %d, got %d", 44169, b)
		}
	} else {
		t.Errorf("Expected int, got %T", solb)
	}
}
