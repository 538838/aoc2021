package d23

import (
	"aoc2021/d1"
	"aoc2021/d16"
	"aoc2021/d5"
	"crypto/md5"
	"io"
)

// Probably not the slimmest or fastest solution

// Run runs the program and returns the results
func Run(input io.Reader) (resA, resB interface{}) {
	linesA := d1.ReadLines(input)
	linesB := make([]string, len(linesA)+2)
	copy(linesB[:3], linesA)
	copy(linesB[3:], []string{"  #D#C#B#A#", "  #D#B#A#C#"})
	copy(linesB[5:], linesA[3:])

	a := parseAmphipods(linesA)
	a.sort()
	resA = a.cost
	b := parseAmphipods(linesB)
	b.sort()
	resB = b.cost
	return
}

// parseAmphipods returns an amphipods struct from input strings
func parseAmphipods(lines []string) *amphipods {
	hallway := make([]int, len(lines[1])-2)
	for i := range hallway {
		hallway[i] = -1
	}

	var rooms [][]int
	var roomToHallway []int
	var roomToCost []int
	roomCost := 1
	for i, r := range lines[2] {
		if r != '#' {
			rooms = append(rooms, make([]int, len(lines)-3))
			roomToHallway = append(roomToHallway, i-1)
			roomToCost = append(roomToCost, roomCost)
			roomCost *= 10
		}
	}
	for d, l := range lines[2 : len(lines)-1] {
		for r, i := range roomToHallway {
			rooms[r][d] = int(l[i+1] - 'A')
		}
	}
	return &amphipods{
		hallway:       hallway,
		rooms:         rooms,
		roomToHallway: roomToHallway,
		roomToCost:    roomToCost,
		cost:          0,
	}
}

// amphipods represents the amphipods state, current cost, the layout and the costs for each room
type amphipods struct {
	hallway       []int
	rooms         [][]int
	cost          int
	roomToHallway []int
	roomToCost    []int
}

// stateHash returns a hash of the amphipods state
func (a *amphipods) stateHash() [16]byte {
	b := make([]byte, 0, len(a.hallway)+len(a.rooms)*len(a.rooms[0]))
	for _, h := range a.hallway {
		b = append(b, byte(h))
	}
	for _, rr := range a.rooms {
		for _, r := range rr {
			b = append(b, byte(r))
		}
	}
	return md5.Sum(b)
}

// sort makes sure that the amphipods ends up in the correct rooms
func (a *amphipods) sort() {
	// Init map of states to test
	next := map[[16]byte]*amphipods{a.stateHash(): a.copy()}
	a.cost = d16.IntMax

	for len(next) > 0 {
		for k, n := range next {
			// Until all states are tested iterate one step (move to rooms and move to hallway)
			delete(next, k)
			if n.cost > a.cost {
				continue
			}
			n.moveToRooms()
			// if the resulting state is sorted, save the result and cost
			if n.sorted() {
				if n.cost < a.cost {
					a.hallway = n.hallway
					a.rooms = n.rooms
					a.cost = n.cost
				}
				continue
			}
			// Else add possible moves to state map
			for _, a := range n.moveToHallway() {
				k = a.stateHash()
				if a2, ok := next[k]; !ok || a2.cost > a.cost {
					next[k] = a
				}
			}
		}
	}
}

// copy creates a copy of this amphipods, deep copying the hallway and rooms state only
func (a *amphipods) copy() (a2 *amphipods) {
	a2 = &amphipods{
		hallway:       make([]int, len(a.hallway)),
		rooms:         make([][]int, len(a.rooms)),
		cost:          a.cost,
		roomToHallway: a.roomToHallway,
		roomToCost:    a.roomToCost,
	}
	copy(a2.hallway, a.hallway)
	for i := range a.rooms {
		a2.rooms[i] = make([]int, len(a.rooms[i]))
		copy(a2.rooms[i], a.rooms[i])
	}
	return a2
}

// lastInRoom returns the highes occupied index in input room, and a bool indicating
// if the room only contains amphipods belonging in the room
func (a *amphipods) lastInRoom(room int) (int, bool) {
	i := 0
	for _, r := range a.rooms[room] {
		if r == -1 {
			i++
		} else if r != room {
			return i, false
		}
	}
	return i, true
}

// hallwayPath returns the hallway distance, or -1 if the path is not free
func (a *amphipods) hallwayPath(from, to int) int {
	di, sgn := d5.AbsSgn(to - from)
	for i := 1; i <= di; i++ {
		if a.hallway[from+i*sgn] != -1 {
			return -1
		}
	}
	return di
}

// sorted returns true if all rooms are filled with amphipods belonging in the rooms
func (a *amphipods) sorted() bool {
	for i := range a.rooms {
		if ri, ok := a.lastInRoom(i); ri != 0 || !ok {
			return false
		}
	}
	return true
}

// moveToRooms moves all amphipods in the hallway to their target room, if the path is clear
func (a *amphipods) moveToRooms() {
outer:
	for {
		for i, h := range a.hallway {
			if h != -1 {
				dh := a.hallwayPath(i, a.roomToHallway[h])
				ri, ok := a.lastInRoom(h)
				if dh != -1 && ok {
					a.hallway[i] = -1
					a.rooms[h][ri-1] = h
					a.cost += (dh + ri) * a.roomToCost[h]
					continue outer
				}
			}
		}
		return
	}
}

// moveToHallway moves all amphipods not beloning in their current room to the hallway, if the path is clear.
// Returns a list of the different resulting states
func (a *amphipods) moveToHallway() (nxt []*amphipods) {
	for i, h := range a.hallway {
		if h == -1 && !intsContain(a.roomToHallway, i) {
			for r := 0; r < len(a.rooms); r++ {
				dh := a.hallwayPath(i, a.roomToHallway[r])
				ri, ok := a.lastInRoom(r)
				if dh != -1 && !ok {
					n := a.copy()
					rv := n.rooms[r][ri]
					n.hallway[i] = rv
					n.rooms[r][ri] = -1
					n.cost += (dh + ri + 1) * a.roomToCost[rv]
					nxt = append(nxt, n)
				}
			}
		}
	}
	return
}

// intsContain returns true if the int slice contains the int
func intsContain(s []int, v int) bool {
	for _, i := range s {
		if v == i {
			return true
		}
	}
	return false
}
